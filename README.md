## Event Management

### Installation

```sh
$ cd /projectfolder
$ composer install
$ generate .env file
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed --class=UserSeeder
$ php artisan db:seed --class=EventSeeder
$ php artisan db:seed --class=EventAttendeesSeeder
$ php artisan serve
$ Visit http://127.0.0.1:8000/api/event
```

## API Calls

### Get ALL Events

`GET /api/event HTTP/1.1`

    Host: 127.0.0.1:8000
	Content-Type: application/x-www-form-urlencoded

### GET Single Event

`GET /api/event/50 HTTP/1.1`

	Host: 127.0.0.1:8000

### Update Event

`PUT /api/event/50 HTTP/1.1`

	Host: 127.0.0.1:8000
	Content-Type: application/x-www-form-urlencoded

	name=Sample&organizer_id=10

### Create Event

`POST /api/event HTTP/1.1`

	Host: 127.0.0.1:8000
	Content-Type: application/x-www-form-urlencoded

	name=Sample&description=Test&date=2022-05-22&start_time=14:00&end_time=15:00&organizer_id=12

### Delete Event

`DELETE /api/event/10 HTTP/1.1`

	Host: 127.0.0.1:8000
	Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

### License
**Open source, Hell Yeah!**