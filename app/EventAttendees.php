<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendees extends Model
{
    public function users()
    {
        return $this->hasMany(User::class, 'user_id', 'id');
    }
}
