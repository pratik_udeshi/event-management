<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = ['id'];

    public function eventsAttendees()
    {
        return $this->hasMany(EventAttendees::class, 'event_id', 'id');
    }

    public function organizer()
    {
        return $this->hasOne(User::class, 'organizer_id', 'id');
    }

}
