<?php

namespace App\Http\Controllers;

use App\EventAttendees;
use App\Event;
use Illuminate\Http\Request;

class EventAttendeesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventAttendees  $eventAttendees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventAttendees  $eventAttendees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
