<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventAttendees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Event::with('eventsAttendees')->latest()->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $eventid
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Event::with('eventsAttendees')->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:events',
            'description' => 'required',
            'date' => 'required|date_format:Y-m-d|after:today',
            'start_time' => 'required',
            'end_time' => 'required',
            'organizer_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $event = new Event();
        $input = $request->all();
        $event->fill($input)->save();

        return response('Event has been created Successfully', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:255|unique:events',
            'date' => 'date_format:Y-m-d|after:today',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $event = Event::findOrFail($id);
        $input = $request->all();
        $event->fill($input)->save();

        return response('Event has been updated Successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        return response('Event has been deleted!', 200);
    }
}
