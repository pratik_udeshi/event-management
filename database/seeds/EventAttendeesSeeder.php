<?php

use Illuminate\Database\Seeder;

class EventAttendeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\EventAttendees::class, 50)->create();
    }
}
