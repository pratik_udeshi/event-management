<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
       'name' => $faker->name,
       'description' => $faker->paragraph,
       'date' => $faker->date($format = 'Y-m-d'),
       'start_time' => $faker->time($format = 'H:i'),
       'end_time' => $faker->time($format = 'H:i'),
       'organizer_id' => $faker->numberBetween(1,50),
    ];
});
