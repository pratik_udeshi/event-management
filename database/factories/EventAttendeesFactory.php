<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EventAttendees;
use Faker\Generator as Faker;

$factory->define(EventAttendees::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1,50),
        'event_id' => $faker->numberBetween(1,50),
    ];
});
